#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, ConfigParser, sleekxmpp
sys.path.append('lib/')

from optparse import OptionParser
from handler import Handler
from admin import Admin
try:
    from pypump import PyPump
except:
    print ('Módulo de Pump no encontrado, no se usará')


if sys.version_info < (3, 0):
    reload(sys)
    sys.setdefaultencoding('utf8')
else:
    raw_input = input

class MUCBot(sleekxmpp.ClientXMPP):
    def __init__(self, dic, dic_pump):
        self.dic = dic
        self.dic_pump = dic_pump
        sleekxmpp.ClientXMPP.__init__(self, self.dic['jid'], self.dic['password'])
        self.room = self.dic['room']
        self.nick = self.dic['nick']
        self.add_event_handler("session_start", self.start)
        self.add_event_handler("muc::%s::got_online" % self.room, self.muc_online) 
        self.add_event_handler("changed_status", self.presence)
        self.add_event_handler("message", self.message)
        self.admin = Admin(self.dic['admins'].split(', '))
        try:
            self.pump = PyPump(self.dic_pump['pump_id'], self.dic_pump['client_key'], self.dic_pump['client_secret'], self.dic_pump['token_key'], self.dic_pump['token_secret'])
        except:
            print ('I did not find any pump configuration, it won\'t be used.')
            self.pump = ""

    def start(self, event):
        self.use_tls = True
        self.get_roster()
        self.send_presence()
        self.plugin['xep_0045'].joinMUC(self.room, self.nick, wait = True)

    def message(self, msg):
        if msg['nick'] != self.nick:
          Handler(msg, self.admin, self.pump)

    def presence(self, presence):
	jid = str(presence['muc']['jid']).split('/')[0]
	status = str(presence['type'])
	self.admin.identify(jid, status)

    def muc_online(self, presence):
        if presence['muc']['nick'] != self.nick:
            self.send_message(mto=presence['from'].bare, mbody="o/ %s" % (presence['muc']['nick']), mtype='groupchat')

def configSectionMap(section):
    dic = {}
    for e in config.items(section):
        dic[e[0]] = e[1]
    return dic

if __name__ == '__main__':
    optp = OptionParser()
    optp.add_option('-c', '--config', dest='config', help='set configuration file location')
    opts, args = optp.parse_args()
    if opts.config is None:
        opts.config = os.getenv("HOME")+'/.alex/config'
    
    config = ConfigParser.RawConfigParser()
    config.read(opts.config)
    dic = configSectionMap('Default')
    dic_pump = configSectionMap('Pump')
    xmpp = MUCBot(dic, dic_pump)
    xmpp.register_plugin('xep_0030')
    xmpp.register_plugin('xep_0045')
    xmpp.register_plugin('xep_0199')

    if xmpp.connect():
        xmpp.process(block=True)
        print('Done')
    else:
        print('Unable to connect.')
