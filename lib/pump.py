#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pypump import PyPump, Client

class Pump:
	def simple_verifier(self, url):
		print 'Abre la siguiente dirección e introduce el Verifier: ' + url
		return raw_input('Verifier: ')

	def __init__(self, pump_id, client_key, client_secret, token_token, token_secret):
		self.client = Client(
			webfinger = pump_id ,
			type = "native",
			name = "Alex",
			key = client_key,
			secret= client_secret
			)
		self.pump = PyPump(
			client = self.client,
			token = token_token,
			secret = token_secret,
			verifier_callback=self.simple_verifier
			)
		
	def publicPost(self, string):
		self.note = self.pump.Note(string)
		self.note.to = self.pump.Public
		self.note.send()
		return self.note.url
	
	def parse(self, string):
		returned = string.replace('.pump', '');
		return returned
