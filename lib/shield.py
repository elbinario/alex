#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Shield:

	def __init__ (self, msg, log):
		""" Class initialiser """
		self.msg = msg
		self.log = log

	def isBeingInjected(self):
		commands = ['&&', '|', ';'] #commands not permitted
		self.injecting = False
		for e in commands:
			if e in self.msg:
				self.injecting = True
		return self.injecting
