#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Log:
	def __init__(self, path):
		self.path = path
		self.open()

	def open(self):
		self.log = open(self.path + 'alex.log', 'a')
		self.ilog = open(self.path + 'injecting.log', 'a')

	def iwrite(self, string):
		self.ilog.writelines(string+'\n')

	def close(self):
		self.log.close()
		self.ilog.close()
