#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Admin:
	def __init__(self, admins):
		self.admins = admins
		self.authorized = [] 
	
	def identify(self, admin, status):
                if admin in self.admins:
                        if not (admin in self.authorized):
                                if status != 'unavailable':
                                        self.authorized.append(admin)
                                        return True
                        else:
                                self.unidentify(admin)
                return False
		
	def unidentify(self, admin):
                print admin
                #x = self.authorized.index(admin)
                #self.authorized[x:x+1] = []
                self.authorized.remove(admin)
                print "unidentified"

	def isLogged(self, admin): #puede morir?
		in_authorized = False
		for e in self.authorized:
			if e == admin:
				in_authorized = True
		return in_authorized
		
	def getAdmins(self):
		return self.authorized
