#!/usr/bin/env python
# -*- coding: utf-8 -*-
from shield import Shield 

class Handler:
	def __init__ (self, msg, admin, pump):
		self.msg = msg
		self.admin = admin
		self.pump = pump
                self.muc_jid = str(msg['to']).split('/')[0]
		self.commands = ['.help', '.pump', '.close', '.lsadmins']
		shield = Shield(msg, True)
		if shield.isBeingInjected():
			print ('Injection detected.')
		else:
			self.handleCommands()
		
	def handleCommands(self):
		dic = {'.help' : self.help, '.pump' : self.pumpPost, '.close' : self.close, '.lsadmins' : self.lsAdmins}
		for e in self.commands:
                        if e == self.msg['body']:
				dic[e](self.msg)
				break

	def help(self, msg):
		string = 'Los comandos disponibles son: ' + ' '.join(self.commands)
		msg.reply(string).send()
				
	def pumpPost(self, msg):
                if self.identify(msg):
                        msg.reply('Hago ver que envio un pumpeo').send()
                        parsed = self.pump.parse(msg['body'])
                        if len(parsed) > 10:
                                url = self.pump.publicPost(parsed)
                                msg.reply('Mensaje publicado en pump: «' + parsed + '» ' + url).send()
                        else:
                                msg.reply('El mensaje debe contener almenos 10 carácteres.').send()
                else:
                        print "No eres administrador, lo siento."

        def identify(self, msg):
                if jid in self.admin.isLogged(jid):
                        return True
				
	def lsAdmins(self, msg):
		if len(self.admin.getAdmins()) > 0:
			string = 'Las administradoras son: ' + ' '.join(self.admin.getAdmins())
		else:
			string = 'No hay administradoras loegueados.'
		msg.reply(string).send()
		
	def close(self, msg):
                #TODO if (msg['nick'] == 'haton'):
                exit()
                
