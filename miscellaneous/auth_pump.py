#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pypump import PyPump, Client

client = Client(
	webfinger=raw_input('Introduce tu id de pump.io (usuario@servidor): '),
	type="native", # Can be "native" or "web"
	name="Alex"
)

def simple_verifier(url):
	print 'Abre la siguiente dirección e introduce el Verifier: ' + url
	return raw_input('Verifier: ')

pump = PyPump(client=client, verifier_callback=simple_verifier)
client_credentials = pump.get_registration() 
client_tokens = pump.get_token() 
print '--Client--'
print 'client_key: ' + client_credentials[0]
print 'client_secret: ' + client_credentials[1]
print '----------'
print '--Token--'
print 'token_key: ' + client_tokens[0]
print 'token_secret: ' + client_tokens[1]
print '----------'
